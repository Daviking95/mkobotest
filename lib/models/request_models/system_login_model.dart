
class SystemLoginData {
  String? _ux;
  String? _iv;
  String? _key;

  SystemLoginData({String? ux, String? iv, String? key}) {
    this._ux = ux;
    this._iv = iv;
    this._key = key;
  }

  String get ux => _ux!;
  set ux(String ux) => _ux = ux;
  String get iv => _iv!;
  set iv(String iv) => _iv = iv;
  String get key => _key!;
  set key(String key) => _key = key;

  SystemLoginData.fromJson(Map<String, dynamic> json) {
    _ux = json['ux'];
    _iv = json['iv'];
    _key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ux'] = this._ux;
    data['iv'] = this._iv;
    data['key'] = this._key;
    return data;
  }
}
