
class CreateProfileModel {
  String? _emailAddress;
  String? _phonePri;

  CreateProfileModel({String? emailAddress, String? phonePri}) {
    this._emailAddress = emailAddress;
    this._phonePri = phonePri;
  }

  String get emailAddress => _emailAddress!;
  set emailAddress(String emailAddress) => _emailAddress = emailAddress;
  String get phonePri => _phonePri!;
  set phonePri(String phonePri) => _phonePri = phonePri;

  CreateProfileModel.fromJson(Map<String, dynamic> json) {
    _emailAddress = json['emailAddress'];
    _phonePri = json['phonePri'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['emailAddress'] = this._emailAddress;
    data['phonePri'] = this._phonePri;
    return data;
  }
}
