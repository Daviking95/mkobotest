
class SystemLoginResponseModel {
  String? _authorization;

  SystemLoginResponseModel({String? authorization}) {
    this._authorization = authorization;
  }

  String get authorization => _authorization!;
  set authorization(String authorization) => _authorization = authorization;

  SystemLoginResponseModel.fromJson(Map<String, dynamic> json) {
    _authorization = json['Authorization'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Authorization'] = this._authorization;
    return data;
  }
}
