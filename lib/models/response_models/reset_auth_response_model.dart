

class ResetAuthResponseModel {
  String? _walletId;
  String? _doFt;
  String? _doBillpay;
  String? _bpLimit;
  String? _ftLimit;
  String? _mobile;
  String? _key;
  String? _iv;
  String? _kyc;

  ResetAuthResponseModel(
      {String? walletId,
        String? doFt,
        String? doBillpay,
        String? bpLimit,
        String? ftLimit,
        String? mobile,
        String? key,
        String? iv,
        String? kyc}) {
    this._walletId = walletId;
    this._doFt = doFt;
    this._doBillpay = doBillpay;
    this._bpLimit = bpLimit;
    this._ftLimit = ftLimit;
    this._mobile = mobile;
    this._key = key;
    this._iv = iv;
    this._kyc = kyc;
  }

  String get walletId => _walletId!;
  set walletId(String walletId) => _walletId = walletId;
  String get doFt => _doFt!;
  set doFt(String doFt) => _doFt = doFt;
  String get doBillpay => _doBillpay!;
  set doBillpay(String doBillpay) => _doBillpay = doBillpay;
  String get bpLimit => _bpLimit!;
  set bpLimit(String bpLimit) => _bpLimit = bpLimit;
  String get ftLimit => _ftLimit!;
  set ftLimit(String ftLimit) => _ftLimit = ftLimit;
  String get mobile => _mobile!;
  set mobile(String mobile) => _mobile = mobile;
  String get key => _key!;
  set key(String key) => _key = key;
  String get iv => _iv!;
  set iv(String iv) => _iv = iv;
  String get kyc => _kyc!;
  set kyc(String kyc) => _kyc = kyc;

  ResetAuthResponseModel.fromJson(Map<String, dynamic> json) {
    _walletId = json['walletId'];
    _doFt = json['doFt'];
    _doBillpay = json['doBillpay'];
    _bpLimit = json['bpLimit'];
    _ftLimit = json['ftLimit'];
    _mobile = json['mobile'];
    _key = json['key'];
    _iv = json['iv'];
    _kyc = json['kyc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['walletId'] = this._walletId;
    data['doFt'] = this._doFt;
    data['doBillpay'] = this._doBillpay;
    data['bpLimit'] = this._bpLimit;
    data['ftLimit'] = this._ftLimit;
    data['mobile'] = this._mobile;
    data['key'] = this._key;
    data['iv'] = this._iv;
    data['kyc'] = this._kyc;
    return data;
  }
}
