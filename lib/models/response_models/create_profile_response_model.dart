
class CreateProfileResponseModel {
  int? _tid;
  String? _phonePri;
  String? _userRole;
  String? _userRoleStr;
  String? _emailAddress;

  CreateProfileResponseModel(
      {int? tid,
        String? phonePri,
        String? userRole,
        String? userRoleStr,
        String? emailAddress}) {
    this._tid = tid;
    this._phonePri = phonePri;
    this._userRole = userRole;
    this._userRoleStr = userRoleStr;
    this._emailAddress = emailAddress;
  }

  int get tid => _tid!;
  set tid(int tid) => _tid = tid;
  String get phonePri => _phonePri!;
  set phonePri(String phonePri) => _phonePri = phonePri;
  String get userRole => _userRole!;
  set userRole(String userRole) => _userRole = userRole;
  String get userRoleStr => _userRoleStr!;
  set userRoleStr(String userRoleStr) => _userRoleStr = userRoleStr;
  String get emailAddress => _emailAddress!;
  set emailAddress(String emailAddress) => _emailAddress = emailAddress;

  CreateProfileResponseModel.fromJson(Map<String, dynamic> json) {
    _tid = json['tid'];
    _phonePri = json['phonePri'];
    _userRole = json['userRole'];
    _userRoleStr = json['userRoleStr'];
    _emailAddress = json['emailAddress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tid'] = this._tid;
    data['phonePri'] = this._phonePri;
    data['userRole'] = this._userRole;
    data['userRoleStr'] = this._userRoleStr;
    data['emailAddress'] = this._emailAddress;
    return data;
  }
}
