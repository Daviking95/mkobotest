
class ErrorResponseData {
  String? _responseDesc;
  String? _iv;
  String? _key;

  ErrorResponseData({String? responseDesc, String? iv, String? key}) {
    this._responseDesc = responseDesc;
    this._iv = iv;
    this._key = key;
  }

  String get responseDesc => _responseDesc!;
  set responseDesc(String responseDesc) => _responseDesc = responseDesc;
  String get iv => _iv!;
  set iv(String iv) => _iv = iv;
  String get key => _key!;
  set key(String key) => _key = key;

  ErrorResponseData.fromJson(Map<String, dynamic> json) {
    _responseDesc = json['responseDesc'];
    _iv = json['iv'];
    _key = json['key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseDesc'] = this._responseDesc;
    data['iv'] = this._iv;
    data['key'] = this._key;
    return data;
  }
}
