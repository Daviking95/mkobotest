import 'package:untitled/models/request_models/create_profile_model.dart';
import 'package:untitled/models/response_models/create_profile_response_model.dart';
import 'package:untitled/models/response_models/reset_auth_response_model.dart';
import 'package:untitled/models/response_models/system_login_response_model.dart';

class ResponseContants {
  static late ResetAuthResponseModel responseModel;

  static late SystemLoginResponseModel systemLoginResponseModel;

  static late CreateProfileResponseModel createProfileModel;
}
