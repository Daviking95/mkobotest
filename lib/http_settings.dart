import 'dart:convert';
import 'dart:io';

class HttpService {
  static Future<dynamic> sendRequestToServer(
      dynamic model, String reqType, String httpUrl,
      {bool isGet = false, bool isTokenHeader = false, String? token}) async {
    print(httpUrl);

    HttpClient client = new HttpClient();
    client.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = isGet
        ? await client.getUrl(Uri.parse(httpUrl))
        : await client.postUrl(Uri.parse(httpUrl));

    request.headers.set('Content-Type', 'application/json');

    request.headers.set('accept', 'text/plain');

    if (isTokenHeader) {
      request.headers.set('Authorization', 'Bearer $token');
    }

    request.add(utf8.encode(jsonEncode(model)));

    HttpClientResponse result = await request.close();

    print("Printing here ${jsonDecode(await result.transform(utf8.decoder).join())}");

    return jsonDecode(await result.transform(utf8.decoder).join());

    // if (result.statusCode == 200) {
    //
    //   print("Printing here ${jsonDecode(await result.transform(utf8.decoder).join())}");
    //
    //   return jsonDecode(await result.transform(utf8.decoder).join());
    // } else {
    //   return jsonDecode(await result.transform(utf8.decoder).join());
    // }
  }
}
