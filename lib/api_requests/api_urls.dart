
class ApiUrls {

  static const String baseUrl = 'http://188.166.164.3:9595';

  static const String resetServicesUrl = '$baseUrl/paysure/api/auth/reset';

  static const String sysLoginServicesUrl = "$baseUrl/paysure/api/auth/sys-login";

  static const String createProfileServicesUrl = "$baseUrl/paysure/api/processor/create-profile";

}