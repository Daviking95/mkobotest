
import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:untitled/constants/response_contants.dart';
import 'package:untitled/encrypt_decrypt.dart';

import 'package:untitled/models/request_models/create_profile_model.dart';
import 'package:untitled/models/response_models/create_profile_response_model.dart';
import 'package:untitled/models/response_models/error_response_model.dart';

import '../../api_urls.dart';

class CreateProfileServices {

  Future<dynamic> createUserProfile() async{

    CreateProfileResponseModel createProfileModel;

    Map<String, String> requestData = {
      "emailAddress": "taysay@vaspay.com",
      "phonePri": "+2348036533888"
    };

    var encodedData = jsonEncode(requestData);

    var encryptedString = await cryptString(encodedData);

    print(encodedData);

    print(ApiUrls.createProfileServicesUrl);

    var uri = Uri.parse(ApiUrls.createProfileServicesUrl);

    var responseData = await http.post(uri, body: encryptedString, headers: {
      "Authorization" : ResponseContants.systemLoginResponseModel.authorization
    });

    print(responseData.body);
    log(responseData.body);

    if(responseData.statusCode != 200 || responseData.statusCode != 202){
      return ErrorResponseData.fromJson(jsonDecode(responseData.body)) ;
    }

    createProfileModel = CreateProfileResponseModel.fromJson(jsonDecode(responseData.body));

    ResponseContants.createProfileModel = createProfileModel;

    return createProfileModel;
  }
}