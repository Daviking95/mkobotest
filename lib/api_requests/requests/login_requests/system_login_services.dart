

import 'dart:convert';
import 'dart:developer';

import 'package:untitled/constants/response_contants.dart';
import 'package:untitled/models/request_models/system_login_model.dart';
import 'package:http/http.dart' as http;
import 'package:untitled/models/response_models/error_response_model.dart';
import 'package:untitled/models/response_models/system_login_response_model.dart';

import '../../api_urls.dart';

class SystemLoginServices {

  Future<dynamic> requestSystemLogin(SystemLoginData systemLoginData) async{

    SystemLoginResponseModel systemLoginResponseModel;

    Map<String, String> requestData = {
      "ux" : systemLoginData.ux,
      "iv" : systemLoginData.iv,
      "key" : systemLoginData.key
    };

    var encodedData = jsonEncode(requestData);

    print(encodedData);

    print(ApiUrls.sysLoginServicesUrl);

    var uri = Uri.parse(ApiUrls.sysLoginServicesUrl);

    var responseData = await http.post(uri, body: encodedData);

    print(responseData.body);
    log(responseData.body);

    if(responseData.statusCode != 200 || responseData.statusCode != 202){
      return ErrorResponseData.fromJson(jsonDecode(responseData.body)) ;
    }

    systemLoginResponseModel = SystemLoginResponseModel.fromJson(jsonDecode(responseData.body));

    ResponseContants.systemLoginResponseModel = systemLoginResponseModel;

    return systemLoginResponseModel;
  }
}