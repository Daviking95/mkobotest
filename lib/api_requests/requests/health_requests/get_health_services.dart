
import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:untitled/constants/response_contants.dart';
import 'package:untitled/models/response_models/error_response_model.dart';
import 'package:untitled/models/response_models/reset_auth_response_model.dart';

import '../../api_urls.dart';

class ResetServices {

  Future<dynamic> requestAuthCredentials() async{

    ResetAuthResponseModel responseModel;

    Map<String, String> requestData = {
      "ux" : "daviking95@gmail.com"
    };

    var encodedData = jsonEncode(requestData);

    print(encodedData);

    print(ApiUrls.resetServicesUrl);

    var uri = Uri.parse(ApiUrls.resetServicesUrl);

    var responseData = await http.post(uri, body: requestData);

    print(responseData.body);
    log(responseData.body);

    if(responseData.statusCode != 200 || responseData.statusCode != 202){
      return ErrorResponseData.fromJson(jsonDecode(responseData.body)) ;
    }

    responseModel = ResetAuthResponseModel.fromJson(jsonDecode(responseData.body));

    ResponseContants.responseModel = responseModel;

    return responseModel;
  }
}