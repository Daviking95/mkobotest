import 'dart:io';

import 'package:flutter/material.dart';
import 'package:untitled/api_requests/requests/login_requests/system_login_services.dart';
import 'package:untitled/models/request_models/system_login_model.dart';

import 'api_requests/requests/create_profile_request/create_profile_services.dart';
import 'api_requests/requests/health_requests/get_health_services.dart';
import 'constants/response_contants.dart';

void main() {
  runApp(MyApp());

  HttpOverrides.global = new MyHttpOverrides();

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadRequests();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Welcome To Playtech',
            ),

          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _loadRequests() async {
    await ResetServices().requestAuthCredentials();

    await SystemLoginServices().requestSystemLogin(_getSystemLoginData());

    await CreateProfileServices().createUserProfile();
  }

  SystemLoginData _getSystemLoginData() {
    SystemLoginData systemLoginData = SystemLoginData(
        ux: "daviking95@gmail.com",
        iv: ResponseContants.responseModel.iv,
        key: ResponseContants.responseModel.key);

    return systemLoginData;
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
